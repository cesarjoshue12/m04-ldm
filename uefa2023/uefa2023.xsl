<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Activitat 1</title>
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>
        <a href="https://www.uefa.com/uefachampionsleague/history/seasons/2023/">
            
            <img src="UEFA/season/organization/logo_uefa" style="height: 75px; witdh: 75px;"/>
        </a>
        <h2>
            <xsl:value-of select="UEFA/season/organization/name"/>
        </h2>
        <table border="1">
            <thead>
                <tr>
                    <th colspan="7">Quarter finals</th>
                    <th>Winner</th>
                </tr>
            </thead>
            <xsl:for-each select="UEFA/season/quarter-finals/match">
            <tbody>
                <tr>
                    <td>
                        <xsl:variable name="imageSrc1" select="first-leg/local/logo"/>
                        <img src="{$imageSrc1}" style="height: 75px; witdh: 75px;"/>
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/local/name" />
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/local/goals" />
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/visitant/goals"/>
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/visitant/name"/>
                    </td>
                    <td>
                        <xsl:variable name="imageSrc2" select="first-leg/local/logo"/>
                        <img src="{$imageSrc2}" style="height: 75px; witdh: 75px;"/>
                    </td>
                    <td rowspan="2">
                        <xsl:variable name="campio" select="winner"/>
                        <xsl:choose>
                            <xsl:when test="first-leg/local/id=$campio">
                                <xsl:variable name="logocampio" select="first-leg/local/logo"/>
                                <img src="{$logocampio}" style="height: 75px; witdh: 75px;"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="logocampio2" select="first-leg/visitant/logo"/>
                                <img src="{$logocampio2}" style="height: 75px; witdh: 75px;"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                </tr>
                <tr>
                    <td>
                        <xsl:variable name="imageSrc3" select="first-leg/visitant/logo"/>
                        <img src="{$imageSrc3}" style="height: 75px; witdh: 75px;"/>
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/visitant/name" />
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/visitant/goals" />
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/local/goals"/>
                    </td>
                    <td>
                        <xsl:value-of select="first-leg/local/name"/>
                    </td>
                    <td>
                        <xsl:variable name="imageSrc2" select="first-leg/visitant/logo"/>
                        <img src="{$imageSrc2}" style="height: 75px; witdh: 75px;"/>
                    </td>
                </tr>
            </tbody>
            </xsl:for-each>
        </table>
        <p>
            Total goals: <xsl:value-of select="sum(//goals)"/>
        </p>
        <p>
            Number of matches: <xsl:value-of select="count(//match)*2"/>
        </p>
        <p>
            Average goals per macth: <xsl:value-of select="round(sum(//goals) div (count(//match)*2))"/>
        </p>
    </body>
</html>
</xsl:template>
</xsl:stylesheet>