<?xml version="1.0" encoding="UTF-8"?> 
<?xml-stylesheet type="text/xsl" href="school.xsl"?>
<school>
    <students>
        <student>
            <image>image1.png</image>
            <name>John Doe</name>
            <email>jdoe@mail.com</email>
        </student>
        <student>
            <image>image2.png</image>
            <name>Peter Pan</name>
            <email>ppan@mail.com</email>
        </student>
    </students>
</school>