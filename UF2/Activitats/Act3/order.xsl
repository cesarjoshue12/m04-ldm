<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/" >
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>XSL Transform</title>
            <meta charset="UTF-8" />
            <link rel="stylesheet" href="style.css"/>
        </head>
        <body>
            <h2>Products with a Price &gt; 25 and price &lt;= 100</h2>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>