﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Activitat 1</title>
        <link rel="stylesheet" href="style.css" />
      </head>
      <body>
      <h1>Film List</h1>
        <table>
          <thead>
            <tr>
              <th>Title</th>
              <th>Language</th>
              <th>Year</th>
              <th>Country</th>
              <th>Genre</th>
              <th>Actors</th>
              <th>Summary</th>
            </tr>
          </thead>
          <tbody>
            <xsl:for-each select="films/film">	
              <xsl:sort select="title"/>		
              <tr>
                
                <td><b><xsl:value-of select="title" /></b></td>	
                <td><xsl:value-of select="title/@lang" /></td>
                <td><xsl:value-of select="year" /></td>
                <td><xsl:value-of select="country" /></td>		
                <td><xsl:value-of select="genre" /></td>
                <td>
                <!-- Option 1: show all data of actor-->
                <!--
                  <xsl:for-each select="actor">
                    <xsl:sort select="."/>
                    <xsl:value-of select="."/> <br/>
                  </xsl:for-each>
                -->

                <!-- Option 2: show all data of actor separated by commas-->
                <!--
                  <xsl:for-each select="actor">
                    <xsl:sort select="."/>
                    <xsl:value-of select="."/> 
                    <xsl:if test="position() != last()">
                      <xsl:text>,</xsl:text>
                    </xsl:if>
                  </xsl:for-each>
                -->
                <!-- Option 3: show only name and surname of actor separated by commas-->
                
                  <xsl:for-each select="actor">
                    <xsl:sort select="."/>
                    <xsl:value-of select="first_name"/>&#160;<xsl:value-of select="last_name"/>
                    <xsl:if test="position() != last()">
                      <xsl:text>,&#160;</xsl:text>
                    </xsl:if>
                  </xsl:for-each>

                </td>		
                <td><xsl:value-of select="summary" /></td>		
              </tr>
            </xsl:for-each>
          </tbody>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>