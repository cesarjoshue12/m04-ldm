<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output method="html" indent="yes"/>

    <xsl:template match="/" >
    <html lang="en" xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <title>XSL Transform</title>
            <meta charset="UTF-8" />
            <link rel="stylesheet" href="style.css"/>
        </head>
        <body>
            <table>
                <thead>
                    <tr>
                        <th>Logo</th>
                        <th>Name</th>
                        <th>Type</th>
                        <th>License</th>
                    </tr>
                </thead>
                <xsl:for-each select="//program">
                    <tr>
                        <td>
                            <xsl:variable name="imageSrc" select="logo"/>
                            <img src="{$imageSrc}" style="height: 75px; witdh: 75px;"/>
                        </td>
                        <td>
                            <xsl:value-of select="name" />
                        </td>
                        <td>
                            <xsl:value-of select="type" />
                        </td>
                        <td>
                            <xsl:value-of select="license" />
                        </td>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>
    </xsl:template>
</xsl:stylesheet>   