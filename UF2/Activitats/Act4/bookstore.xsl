<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<title>Activitat 4 Bookstore</title>
		<link rel="stylesheet" href="style.css" />
	</head>
	<body>
	<h1>Bookstore</h1>
	<div class="content">
	<div class="books">
		
		<xsl:for-each select="bookstore/book">
		<xsl:sort select="@category"/>
				
			<table>
				<tr>
					<th>
						Title (lang: <xsl:value-of select="title/@lang"/>)
					</th>
					<td>
						<xsl:value-of select="title"/>
					</td>
				</tr>
				<tr>
					<th>
						Category
					</th>
					<td>
						<xsl:value-of select="@category"/>
					</td>
				</tr>

				<tr>
					<th>
						Year
					</th>
					<td >
						<xsl:value-of select="year"/>
					</td>
				</tr>
				<tr>
					<th>
						Price
					</th>
					<td >
						<xsl:value-of select="price"/>&#160;<xsl:value-of select="price/@currency"/>
					</td>
				</tr>
				<tr>
					<th>
						Format
					</th>
					<td >
						<xsl:value-of select="format/@type"/>
					</td>
				</tr>
				<tr>
					<th>
						ISBN
					</th>
					<td>
						<xsl:value-of select="isbn"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="authors" >
						<b>Authors:</b>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<xsl:for-each select="author">
						<xsl:sort select="."/>
							<xsl:value-of select="."/> <br/>
						</xsl:for-each>
					</td>
				</tr>		
			</table>
					
			</xsl:for-each>
		
		</div>
		</div>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>