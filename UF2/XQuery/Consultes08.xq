(:Comment:)
for $i in doc('institut.xml')/institut/alumnes/alumne
where $i/edat>20
order by $i/nom
return <result>{($i/nom, $i/cognoms, $i/edat)/data()}</result>