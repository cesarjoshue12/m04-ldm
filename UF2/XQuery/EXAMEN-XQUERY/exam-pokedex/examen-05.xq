<pokemons>
{
for $i in doc('pokedex.xml')/pokedex/pokemon
where $i/baseStats/ATK > 100
return
  <pokemon>
    {$i/species}
    {$i/baseStats/ATK}
  </pokemon>
}
</pokemons>
