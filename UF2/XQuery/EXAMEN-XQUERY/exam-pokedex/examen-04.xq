<pokemons>
{
for $i in doc('pokedex.xml')/pokedex/pokemon
where $i/types/type="FLYING"
return
  <pokemon>
    {$i/species}
    {$i/types}
  </pokemon>
}
</pokemons>