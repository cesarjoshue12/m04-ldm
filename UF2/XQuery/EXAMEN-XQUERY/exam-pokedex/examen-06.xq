<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <title>pokemon</title>
    <link rel="stylesheet" href="pokemon.css"/>
</head>
<body>
    <h1>Pokemons</h1>
    <table>
        <tr>
            <th>Image</th>
            <th>Pokemon</th>
            <th>HP</th>
            <th>ATK</th>
            <th>DEF</th>
            <th>SPD</th>
            <th>SATK</th>
            <th>SDEF</th>
        </tr>
        {
            for $i in doc('./pokedex.xml') /pokedex/pokemon
            let $image := $i/image
            return 
            <tr>
                <td>
                    <img src="{data($image)}" width="200"></img>
                </td>
                <td>{$i/species}</td>
                <td>{$i/baseStats/HP}</td>
                <td>{$i/baseStats/ATK}</td>
                <td>{$i/baseStats/DEF}</td>
                <td>{$i/baseStats/SPD}</td>
                <td>{$i/baseStats/SATK}</td>
                <td>{$i/baseStats/SDEF}</td>
            </tr>
        }
    </table>
</body>
</html>