<pokemons>
{
for $i in doc('pokedex.xml')/pokedex/pokemon
order by $i/species
return
  <pokemon>
    {$i/species}
    {$i/abilities}
  </pokemon>
}
</pokemons>