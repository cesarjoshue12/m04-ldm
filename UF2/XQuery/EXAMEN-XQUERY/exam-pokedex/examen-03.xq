<pokemons>
{
for $i in doc('pokedex.xml')/pokedex/pokemon
order by $i/species
return
  <pokemon>
    {$i/species}
    <total>sum({$i/baseStats/*})</total>
  </pokemon>
}
</pokemons>