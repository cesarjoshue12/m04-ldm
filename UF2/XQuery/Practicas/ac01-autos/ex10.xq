for $i in doc('Autos.xml')/autos/vehicles/vehicle
where $i/price>20000
order by $i/price 
return concat(data($i/price),"-",data($i/model))