for $i in doc('Autos.xml')/autos/vehicles/vehicle
where $i/year>2019 and $i/year<2022
return ($i/model,$i/year)