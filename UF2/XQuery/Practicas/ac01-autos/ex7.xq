for $i in doc('Autos.xml')/autos/vehicles/vehicle
where $i/year>2007
order by $i/model descending
return ($i/model,$i/year)