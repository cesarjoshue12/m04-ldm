for $i in doc('Autos.xml')/autos/vehicles/vehicle
where $i/year=2010 or $i/year=2020
order by $i/model
return ($i/model,$i/year)