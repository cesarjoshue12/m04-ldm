for $i in doc('Autos.xml')/autos/vehicles/vehicle
where $i/price
order by number($i/price)
return (data($i/price),data($i/model))