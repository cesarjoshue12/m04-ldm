let $doc:=doc('playstore.xml')
return(
<offer>
{
  for $i in $doc/playstore/games/game
  where $i/price<50
  order by $i/price
  return($i/name)
}
</offer>,
<standard>
{
  for $i in $doc/playstore/games/game
  where $i/price>=50 
  order by $i/price
  return ($i/name)
}

</standard>)

