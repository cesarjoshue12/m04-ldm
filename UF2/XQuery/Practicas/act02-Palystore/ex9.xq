declare function local:expenses($price as xs:decimal?)
as xs:decimal?
{
  let $iva := $price * 0.21
  let $transportTax := 2.5
  return round($iva + $transportTax, 2)
};

for $game in doc("playstore.xml")/playstore/games/game
return 
  <product>
    {$game/name}
    {$game/price}
    <expenses>{local:expenses($game/price)}</expenses>
  </product>


