let $doc:=doc('playstore.xml')
return
<adult_game>
{
  for $i in $doc/playstore/games/game
  where $i/age >= 18
  return ($i/name,$i/price)
}

</adult_game>

